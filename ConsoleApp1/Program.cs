﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            int num1, num2, num3, x, y, z;
            Double num4, num5, num6;
            Console.WriteLine("Input number");
            num1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Input second number");
            num2 = Convert.ToInt32(Console.ReadLine());

            num3 = num1 + num2;
            Console.WriteLine($"{num1}+{num2} = {num3}");
            Console.WriteLine();

            Console.WriteLine("---------------------------------------------------------------------------------");
            Console.WriteLine("Exercises 3");

            Console.WriteLine("Input number");
            num1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Input second number");
            num2 = Convert.ToInt32(Console.ReadLine());

            num3 = num1 / num2;
            Console.WriteLine($"{num1}/{num2} = {num3}");
            Console.WriteLine();

            Console.WriteLine("---------------------------------------------------------------------------------");
            Console.WriteLine("Exercises 4");

            Console.WriteLine("Input number");
            num1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Input second number");
            num2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"First number {num1}");
            Console.WriteLine($"Second number {num2}");

            num3 = num1 * num2;
            Console.WriteLine($"{num1} x {num2} = {num3}");

            Console.WriteLine("---------------------------------------------------------------------------------");
            Console.WriteLine("Exercises 5");

            Console.WriteLine("Input number");
            num1 = Convert.ToInt32(Console.ReadLine());

            for (int j = 1; j <= 10; j++)
                 {
                    Console.WriteLine("{0}*{1}={2}", j, num1, (j*num1));
                 }
            Console.WriteLine("---------------------------------------------------------------------------------");
            Console.WriteLine("Exercises 6");

            Console.WriteLine("enter in the Radius for the area and perimeter");
            num4 = Double.Parse(Console.ReadLine());
            Console.WriteLine("Area is {0} * {1} = {2}", (num4*num4), Math.PI, ((num4*num4)*Math.PI));
            Console.WriteLine("perimeter is {0} * {1} * {2} = {3}", num4, Math.PI, 2,(num4*Math.PI*2));

            Console.WriteLine("---------------------------------------------------------------------------------");
            Console.WriteLine("Exercises 7");
    
            Console.WriteLine("Input number X");
            x = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Input second number Y");
            y = Convert.ToInt32(Console.ReadLine());
            
            Console.WriteLine($" befor X = {x} and Y = {y}");
            z = x;
            x = y;
            y = z;
            Console.WriteLine($" after X = {x} and Y = {y}");

            Console.WriteLine("---------------------------------------------------------------------------------");
            Console.WriteLine("Exercises 8");

            Console.WriteLine("Input number");
            num4 = Double.Parse(Console.ReadLine());
            Console.WriteLine("Input second number");
            num5 = Double.Parse(Console.ReadLine());

            if (num4 == num5)
                {
                    Console.WriteLine($"{num4} and {num5} are equal");               
                }
            else if (num4 > num5)
                {
                    Console.WriteLine($"{num4} is greater {num5}");
                    Console.WriteLine($"{num4} and {num5} are not equal ");
                }
            else
                {
                    Console.WriteLine($"{num4} is less than {num5}");
                    Console.WriteLine($"{num4} and {num5} are not equal ");
                }

            Console.WriteLine("---------------------------------------------------------------------------------");
            Console.WriteLine("Exercises 9");

            Console.WriteLine("Input number");
            num4 = Double.Parse(Console.ReadLine());
            
            if (num4 > 0)
                {
                    Console.WriteLine($"{num4} is possitive");
                }
            else 
                {
                    Console.WriteLine($"{num4} is negitive");
                }

            Console.WriteLine("---------------------------------------------------------------------------------");
            Console.WriteLine("Exercises 10");

            Console.WriteLine("Input number");
            num4 = Double.Parse(Console.ReadLine());
            Console.WriteLine("Input second number");
            num5 = Double.Parse(Console.ReadLine());
            Console.WriteLine("Input thrid number");
            num6 = Double.Parse(Console.ReadLine());

            if (num4 > num5)
                {
                    if (num4 > num6)
                        {
                            Console.WriteLine($"{num4} is the greatest");
                        }
                    else
                        {
                            Console.WriteLine($"{num6} is the greatest");
                        }
                }
            else if (num5 > num6)
                {
                    Console.WriteLine($"{num5} is the greatest");
                }
            else 
                {
                    Console.WriteLine($"{num6} is the greatest");
                }
            Console.ReadLine();
        }
    }
}
